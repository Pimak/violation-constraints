package net.tncy.mma.validator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;


public class ISBNValidatorTest {

    private ISBNValidator validator;

    @Before
    public void setup(){
        this.validator = new ISBNValidator();
    }

    @Test
    public void ISBNValidTest(){
        assertTrue(validator.isValid("978-316-148-410-0",null));
    }

    @Test
    public void ISBNInvalidTest(){
        assertFalse(validator.isValid("978-316-148-410-1", null));
    }

    @Test
    public void ISBNNullTest(){
        assertFalse(validator.isValid(null, null));
    }

    @Test
    public void ISBNInvalidCharacters(){
        assertFalse(validator.isValid("123456789012a", null));
    }

    @Test
    public void ISBNInvalidXCharacter(){
        assertFalse(validator.isValid("X123456789012", null));
    }

    @Test
    public void ISBNIncorrectLength(){
        assertFalse(validator.isValid("12", null));
    }
}
