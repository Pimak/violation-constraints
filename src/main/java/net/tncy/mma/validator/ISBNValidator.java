package net.tncy.mma.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ISBNValidator implements ConstraintValidator<ISBN, String> {

    @Override
    public void initialize(ISBN constraintAnnotation) {
        // Ici le validateur peut accéder aux attribut de l’annotation.

    }

    @Override
    public boolean isValid(String bookNumber, ConstraintValidatorContext constraintContext) {
        // Algorithme de validation du numéro ISBN

        if (bookNumber == null){
            return false;
        }

        bookNumber = bookNumber.replace("-", "");
        bookNumber = bookNumber.replace(".","");

        if (bookNumber.length()!=13){
            return false;
        }

        int sum = 0;

        for (int i = 0; i<13;i++){
            int valeur = toInt(bookNumber.substring(i,i+1));
            if (valeur<0) {
                return false;
            }
            if (valeur>9){
                return false;
            }
            sum+= ((i%2)*2+1)*valeur;
        }
        return sum%10 == 0;
    }

    private static int toInt(String c){
        switch (c){
            case "0": case "1": case "2": case "3" : case "4": case "5": case "6": case "7": case "8": case "9": return Integer.parseInt(c);
            case "X": return 10;
            default: return -1;
        }
    }

}
